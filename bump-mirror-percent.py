import requests
import re

# envs
DEV = "dev"
QA3 = "qa3"
PROD_COMPARK = "prod-compark"
PROD_CORNELL = "prod-cornell"

# actions
QUIT = "quit"
CHECK_MIRROR_PERCENT = "check_mirror_percent"
SET_MIRROR_PERCENT = "set_mirror_percent"
SET_PATH = "/sm/session/update-manager-tracking"
GET_PATH = "/sm/session/get-manager-tracking"
POST_VERB = "POST"
GET_VERB = "GET"

target_hosts = {
    DEV: "snd031 snd046 snd047 snd067 snd108 snd109".split(" "),
    QA3: "con001 con002 con003 con004 con005".split(" "),
    PROD_COMPARK: "con002 con005 con006 con007 con008 con009 con010 con011 con012 con013 con014 con015 con016 con017 con018 con019 con020 con021".split(" "),
    PROD_CORNELL: "con001 con002 con003 con004 con005 con006 con007 con008 con009 con010 con011 con012 con013 con014 con015 con016 con017 con018 con019 con020".split(" ")
}
actions = f"{CHECK_MIRROR_PERCENT} {SET_MIRROR_PERCENT} {QUIT}".split(" ")

def main() :
    selected_action = choose_action()
    while(selected_action != "quit") :
        do_action(selected_action)
        selected_action = choose_action()

def choose_action() :
    return choose_menu_option("What would you like to do today?", actions)

def do_action(selected_action) :
    print ("You selected : " + ' '.join(x.capitalize() or '_' for x in selected_action.split('_')))
    if (selected_action == CHECK_MIRROR_PERCENT) :
        check_mirror_percent()
    elif (selected_action == SET_MIRROR_PERCENT) :
        set_mirror_percent()
    else :
        print ("Please choose a valid action")

def choose_hosts() :
    env = choose_menu_option("Please choose an env", list(target_hosts.keys()))
    return format_hosts(env, choose_menu_option("Please choose a host by entering a number", target_hosts[env], 'multiple'))

def check_mirror_percent() :
    host = choose_hosts()
    call_hosts(GET_VERB, GET_PATH, host)

def call_hosts(verb, path, hosts) :
    print("\n")
    for host in hosts : 

        try: 
            r = requests.request(verb, f'http://{host}:8080{path}', timeout=(2,2))
            status = r.status_code
            response = r.text
        except requests.exceptions.RequestException as e:
            status = 0
            response = type(e).__name__
    
        if status == 0 : 
            print (host + " : Error, message is " + response)
            continue
        elif status != 200 :
            print (host + " : Error, status code was " + str(r.status_code))
            continue

        response = response.replace("get method not available on manager", "Mirror is not enabled on this server")
        
        print(host + " : " + response)
    
    print("\n\n")

def set_mirror_percent() :
    print ("Set your value in CMT first. Then choose a host to update the mirror-percent on")
    hosts = choose_hosts()
    call_hosts(POST_VERB, SET_PATH, hosts)

# def set_host_mirror_percent(hosts) :
#     r = requests.post("http://" + host + ":8080/sm/session/update-manager-tracking")

#     print ("\nrequest to set the mirror percent sent. The response was:\n" + r.text)

def makeInt(str) : 
    try: 
        return int(str)
    except ValueError: 
         return -1

# freely given by Andre :-)
def choose_menu_option(label, options_list, select_type="single") :
    local_options_list = options_list.copy()

    selected_idx_set = {-1}

    if select_type != "single" : 
        label = label + " (csv for multiple)"
        local_options_list.append("All")

    print (label)

    while selected_idx_set == {-1} : 
        for i in local_options_list:
            print(f"{local_options_list.index(i) + 1}) {i}")

        response = input()
        
        selected_idx_set = set(list(map(makeInt, response.split(","))))
        
        max_len = len(local_options_list)
    
        possible_answers_set = set(range(1, max_len +1))
    
        if not selected_idx_set.issubset(possible_answers_set) :
            print (f"Please try again with a response between 1 and {max_len}")
            selected_idx_set = {-1}

    if select_type == "single" : 
        return local_options_list[selected_idx_set.pop() - 1]

    answers = list(map(lambda itm : local_options_list[itm - 1], selected_idx_set));

    if len(answers) == 1 and answers[0] == "All" : 
        return options_list;

    return answers
 

def format_hosts(env, hosts):
    formatted_hosts = []
    for host in hosts : 
        if (env == DEV):
            formatted_hosts.append(f"du010{host}.dev.angi.cloud")
        elif (env == QA3):
            formatted_hosts.append(f"qu003{host}.homeadvisor.com")
        elif (env == PROD_COMPARK):
            formatted_hosts.append(f"pu020{host}.compark.homeadvisor.com")
        elif (env == PROD_CORNELL):
            formatted_hosts.append(f"pu000{host}.homeadvisor.com")
        else:
            raise ("Invalid env chosen")
    return formatted_hosts

main()
