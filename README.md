### Install python 3 and dependencies
Install python 3 however you'd like to (homebrew, download, etc.)

Python 3 version: 3.9.7

```
python3 -m pip install requests
```

### Running `bump-mirror-percent.py`
The script will give you options to check the mirror percentage for a given host, or to bump it based on the value set in CMT. **Keep in mind that you'll need to set the `percent.to.mirror` and `is.usm.test.active` properties from the `usm` component in CMT (search for usm) for the host environment you'd like to bump.** Also, you need to be on VPN.

```
python3 bump-mirror-percent.py
```